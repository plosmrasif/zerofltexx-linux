# zerofltexx-linux

Get ubuntu or plasma mobile running on your S6

## Getting Started
....

### Prerequisites

```
* Samsung S6
* PC
* Time
```

### Progress
- [x] Create manifest
- [x] Boot image and system image build successfully
- [x] Device boots into rootfs, usb: Manufacturer: GNU/Linux Device appears in dmesg on host.
- [x] LXC container starts and does not crash
- [ ] libhybris tests
   - [ ] test_gps
   - [ ] test_hwcomposer
   - [x] test_lights
   - [x] test_vibrator
   - [x] test_wifi
   - [ ] test_sensors
   - [ ] test_audio
   - [ ] test_camera
   - [ ] test_input
   - [ ] test_nfc
   - [ ] test_recorder

### Features

| **Features** | Lineage | zero-linux |
| ---      |  ------  |----------|
| **Kernel** | 3.10.61  | 3.10.101   |

### Installing
Download:

[halium-boot.img](https://gitlab.com/techfreak/zerofltexx-linux/-/jobs/artifacts/master/download?job=build_boot)  
[system.img](https://gitlab.com/techfreak/zerofltexx-linux/-/jobs/artifacts/master/download?job=build_system) (**Currently broken ! You have to do the building steps for it.**)

Flash the halium-boot.img with heimdall:
```
 heimdall flash --BOOT halium-boot.img;
```
Download a [rootfs](https://ci.ubports.com/job/xenial-rootfs-armhf/lastSuccessfulBuild/artifact/out/ubports-touch.rootfs-xenial-armhf.tar.gz)  and install it with [halium-install](https://gitlab.com/JBBgameich/halium-install)
```
 git clone https://gitlab.com/JBBgameich/halium-install
 cd halium-install
 halium-install -p ut ~/Downloads/ubports-touch.rootfs-xenial-armhf.tar.gz  ~/<halium/out/target/product/zerofltexx>/system.img
```
**If you run in to problems check the [halium-docs](https://docs.halium.org/en/latest/index.html) first and if that does not solve your problems create an issue.**

### Building

To get the build enviroment with these S6 files in it run these commands:
```
git clone https://gitlab.com/techfreak/zerofltexx-linux
cd zerofltexx-linux
repo init -u https://github.com/Halium/android -b halium-7.1 --depth=1
repo sync -c -j 16
```

Setup the device:
```
./halium/devices/setup zerofltexx
source build/envsetup.sh
breakfast zerofltexx
```

Build the halium-boot:
```
cat kernel/samsung/exynos7420/arch/arm64/configs/tech.patch >> kernel/samsung/exynos7420/arch/arm64/configs/lineageos_zerofltexx_defconfig
mka halium-boot
```

Build the system.img
```
export USE_HOST_LEX=yes;
mka systemimage
```

Now you can do the install steps.

## **Disclaimer**
```
* Flash it at your own Risk!
* Your warranty is VOID Knox flags 0x1
* Read instructions carefully
* I'm not responsible for damage to any dead equipment or loss of warranty!
```

## Authors

* [techfreak](https://gitlab.com/techfreak)


## License


## Acknowledgments
* #halium:matrix.org for helping me to fix errors
* Halium(https://halium.org) for there awesome work
* Billie Thompson (https://github.com/PurpleBooth) for this Readme.md template

